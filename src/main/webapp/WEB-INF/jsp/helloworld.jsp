<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
  <head>
    <title>Greeting</title>
    <link rel="stylesheet" type="text/css" href="/style.css">
  </head>
  <body>
    <form action="/isproject/hello" method="post" enctype="multipart/form-data">
      <p>
        <label for="name">What's your name?</label>
        <input id="name" name="name" value="${fn:escapeXml(param.name)}">
        <span class="error">${messages.name}</span>
      </p>
    </form>
  </body>
</html>
