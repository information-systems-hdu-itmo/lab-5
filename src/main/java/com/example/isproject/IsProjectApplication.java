package com.example.isproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(IsProjectApplication.class, args);
	}

}
